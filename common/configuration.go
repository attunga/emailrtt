// Copyright © 2018 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package common

import (
	"github.com/spf13/viper"
)

// Set up Configuration Settings that matter to The processing of new emails
type Configuration struct {
	MaildirFolder          string // the folder holding the mail directory
	DatabaseType           string // The database to use - this will be development, test or production.
	DatabaseConfigLocation string // The location of the database configuration file
}

// Public Load Method to Load Config Values on Demain .. first Struct Method .. woo hoo!!
func (configuration Configuration) Load() Configuration {

	configuration.MaildirFolder = viper.GetString("MaildirFolder")
	configuration.DatabaseType = viper.GetString("DatabaseType")

	return configuration

}
