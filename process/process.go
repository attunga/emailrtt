// Copyright © 2018 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package process

import (
	"emailrtt/common"
	"emailrtt/models"
	"fmt"
	"github.com/gobuffalo/pop"
	"github.com/luksen/maildir"
	"log"
	"net/mail"
	"os"
	"strings"
	"time"
)

func ProcessNewEmails() {

	// Initialise Configuration with Defaults
	config := common.Configuration{".", "development", "not set"}
	config = config.Load() // Hopefully this loads the config

	//	fmt.Println("Config-MailDirFolder", config.MaildirFolder)
	//	fmt.Println("Config.DatabaseType", config.DatabaseType)
	//	fmt.Println("Config.DatabaseConfigLocation", config.DatabaseConfigLocation)

	// Location of the Mail Dir of the user receiving emails
	var mailDir = maildir.Dir(config.MaildirFolder)

	// Count the number of new mails waiting to be processed,  this makes sure the directory exists through the error
	// and if it is 0 then we can just back out graciously
	unseenCountNumber, _ := mailDir.UnseenCount()

	// If Unseen Number is Greater than 0 then get a list of email keys and move them to the current directory
	if unseenCountNumber < 1 {
		fmt.Println("No Messages Found to Process")
		os.Exit(0)
	}

	fmt.Println(unseenCountNumber, "Messages Found - Processing New Messages")

	unseenMessageKeys, err := mailDir.Unseen()

	if err != nil {
		fmt.Println("Error Getting Message Keys")
		os.Exit(1)
	}

	EmailRTTValues := models.EmailRTTValues{} // Create a Slice to hold the EmailRTT Values

	for _, messageKey := range unseenMessageKeys {

		//fmt.Println("Nessage Key:", messageKey)

		currentMessageHeader, _ := mailDir.Header(messageKey)

		// Add new Struct to Slice
		EmailRTTValues = append(EmailRTTValues, processMessageHeader(currentMessageHeader, messageKey))
	}

	fmt.Println("Size of slice is:", len(EmailRTTValues))

	// Commit the results to Database
	var commitResult = commitToDatabase(EmailRTTValues, config.DatabaseType)

	fmt.Println(commitResult)

}

func processMessageHeader(headers mail.Header, messageKey string) models.EmailRTTValue {

	// Reject Email if it did not come from an expected location

	currentRTT := models.EmailRTTValue{}

	// Set the Sent time to the date in the header .. which is the earliest time.
	currentRTT.SentTimeDate, _ = headers.Date()

	currentRTT.MessageKey = messageKey // email Message key

	for key, value := range headers {
		//fmt.Println("Key", key, "Value", value)

		switch key {

		case "From":
			currentRTT.EmailFrom = strings.Trim(value[0], " ")
		case "To":
			currentRTT.EmailTo = strings.Trim(value[0], " ")
		case "Received":
			currentRTT.ReceivedTimeDate = getReceivedTime(value[0])

		}

	}

	// Check there are values in each of the times ... but for now

	// We now do a few custom fields that should make reporting easier at a later date
	currentRTT.TransitTimeSeconds = int(currentRTT.ReceivedTimeDate.Sub(currentRTT.SentTimeDate).Seconds())
	currentRTT.Agency = getAgency(currentRTT.EmailFrom) // Detect the Agency from the Fron address
	currentRTT.ReceivedYear = currentRTT.ReceivedTimeDate.Year()
	currentRTT.ReceivedMonth = currentRTT.ReceivedTimeDate.Month().String()

	// Enable for Debugging
	//showEmailInfo(currentRTT)

	return currentRTT

}

func getAgency(agencyEmail string) string {

	agencyDomain := strings.Split(agencyEmail, "@")

	agency := "UNKNOWN"

	switch agencyDomain[1] {

	case "ag.gov.au":
		agency = "AGD1"
	case "agd.gov.au":
		agency = "AGD2"
	case "afma.gov.au":
		agency = "AFMA"
	case "afsa.gov.au":
		agency = "AFSA"
	case "alrc.gov.au":
		agency = "ALRC"
	case "finance.gov.au":
		agency = "DOF"
	case "border.gov.au":
		agency = "DIBP"
	}

	return agency
}

func getReceivedTime(receivedHeader string) time.Time {

	receivedHeadersSplit := strings.Split(receivedHeader, ";")

	splitTime := strings.Trim(receivedHeadersSplit[1], " ")

	//fmt.Println("Split Time:", splitTime)

	//layout := "Mon, 01/02/06, 03:04PM"

	layout := "Mon, 2 Jan 2006 15:04:05 -0700 (MST)"
	t, err := time.Parse(layout, splitTime)
	if err != nil {
		fmt.Println(err)
	}

	//fmt.Println("Detected Time:", t)

	return t
}

func commitToDatabase(EmailRTTValues models.EmailRTTValues, databaseType string) string {

	// Do a database connection.  Database Type refers to Production, Test or Development
	db, err := pop.Connect(databaseType)
	common.CheckErrorPanic("Fatal error cannot connect to database: %s \n", err)

	err = db.Open()
	common.CheckErrorPanic("Fatal error cannot open database: %s \n", err)

	// Commit to Database
	err = db.Create(EmailRTTValues)
	if err != nil {
		//FIXME - Need to customise error
		log.Panic(err)
		return "Could not create new Records"
	}

	err = db.Close()
	if err != nil {
		//FIXME - Need to customise error
		log.Panic(err)
		return "Could not Close Database"
	}

	return "Database Commit Successful"
}

func showEmailInfo(currentRTT models.EmailRTTValue) {

	fmt.Println("Start #################################")
	fmt.Println("Agency:", currentRTT.Agency)
	fmt.Println("From:", currentRTT.EmailFrom)
	fmt.Println("To:", currentRTT.EmailTo)
	fmt.Println("Sent Time:", currentRTT.SentTimeDate)
	fmt.Println("Received Time:", currentRTT.ReceivedTimeDate)
	fmt.Println("Transit Time Seconds:", currentRTT.TransitTimeSeconds)
	fmt.Println("Received Month:", currentRTT.ReceivedMonth)
	fmt.Println("Received Year:", currentRTT.ReceivedYear)
	fmt.Println("End ###################################")

	fmt.Println("Current Month:", time.Now().Month().String())
	fmt.Println("Previous Month:", time.Now().AddDate(0, -1, 0).Month().String())
	fmt.Println("Current Year:", time.Now().Year())

}
