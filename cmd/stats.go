// Copyright © 2018 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"emailrtt/stats"
	"github.com/spf13/cobra"
)

// statsCmd represents the stats command
var statsCmd = &cobra.Command{
	Use:   "stats",
	Short: "Stats is used to return integers used for monitoring functions",
	Long: `Use the stats command to query the database and return statistics on values in the system
or database that can be used for automated and monitoring of the system:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	//Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("stats called")
		stats.Stats(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(statsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// statsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// statsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	statsCmd.Flags().BoolP("yesterday", "y", false, "The number of emails processed into the database yesterday")
	statsCmd.Flags().BoolP("over", "o", false, "The number of emails that are over 300 seconds RTT yesterday")
	statsCmd.Flags().BoolP("unknown", "u", false, "The number of emails with an UNKNOWN Sender processed into the database yesterday")
	statsCmd.Flags().BoolP("average", "a", false, "The average RTT of the last 7 emails received.")
}
