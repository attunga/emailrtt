package stats

import (
	"emailrtt/common"
	"emailrtt/models"
	"fmt"
	"github.com/gobuffalo/pop"
	"github.com/spf13/cobra"
	"os"
	"time"
)

func Stats(cmd *cobra.Command, args []string) {

	//fmt.Println("into stats")

	// Initialise Configuration with Defaults
	config := common.Configuration{".", "development", "not set"}
	config = config.Load() // Hopefully this loads the config

	// Get value for yesterday which most commands functions use.
	// Using Value of GMT+10 plus an hour to allow for processing .. could try and get Timezone
	// Basically -24 + -12
	yesterdayDate := time.Now().Add(-36 * time.Hour)

	//returnResult := 0

	// Count of Emails in the last day in the database
	if cmd.Flag("yesterday").Value.String() == "true" {

		returnResult := getEmailCountInDB(config, "yesterday", yesterdayDate)

		fmt.Println(returnResult)
		os.Exit(0)
	}

	// Look for numbers of Unknown Agencies
	if cmd.Flag("unknown").Value.String() == "true" {

		returnResult := getEmailCountInDB(config, "unknown", yesterdayDate)

		fmt.Println(returnResult)
		os.Exit(0)
	}

	// Detect RTT Figures in the last day over 300
	if cmd.Flag("over").Value.String() == "true" {

		returnResult := getEmailCountInDB(config, "over", yesterdayDate)

		fmt.Println(returnResult)
		os.Exit(0)
	}

	// Get the average RTT Figures from the last 7 entries
	if cmd.Flag("average").Value.String() == "true" {

		returnResult := getEmailCountInDB(config, "average", yesterdayDate)

		fmt.Println(returnResult)
		os.Exit(0)
	}

	//  Will it actually get to here? .... cmd flags should pick this up.
	//  Maybe Bomb out with an error anyway
	fmt.Println("-1")
	os.Exit(2)

	//fmt.Println(returnResult)
}

func getEmailCountInDB(config common.Configuration, searchType string, yesterdayDate time.Time) int {

	// Get yesterdays date

	var EmailsFound models.EmailRTTValues
	var returnValue int = -111

	// Do a database connection.  Database Type refers to Production, Test or Development
	db, err := pop.Connect(config.DatabaseType)
	common.CheckErrorPanic("Fatal error cannot connect to database: %s \n", err)

	err = db.Open()
	common.CheckErrorPanic("Fatal error cannot open database: %s \n", err)
	defer db.Close()

	searchDate := yesterdayDate.Format("2006-01-02%")

	//fmt.Println(searchDate)

	// Do a query on the database for the year, month and day year of the RTT Records that we want
	switch searchType {
	case "yesterday":
		err = db.RawQuery("select received_time_date from email_r_t_t_values where received_time_date like ?", searchDate).All(&EmailsFound)
		returnValue = len(EmailsFound)
	case "unknown":
		err = db.RawQuery("select received_time_date from email_r_t_t_values where received_time_date like ?"+
			" and agency = ?", searchDate, "UNKNOWN").All(&EmailsFound)
		returnValue = len(EmailsFound)
	case "over":
		err = db.RawQuery("select received_time_date from email_r_t_t_values where received_time_date like ? "+
			" and transit_time_seconds > 300", searchDate).All(&EmailsFound)
		returnValue = len(EmailsFound)
	case "average":
		err = db.RawQuery("select * from email_r_t_t_values ORDER by " +
			"received_time_date desc limit 7").All(&EmailsFound)
		returnValue = getAverageRTT(EmailsFound)
	}

	common.CheckErrorPanic("Fatal error cannot do Query: %s \n", err)

	err = db.Close()
	common.CheckErrorPanic("Fatal error cannot close database: %s \n", err)

	// Return the size of the result

	return returnValue
}

// Get the Average RTT from a group of emails passed
func getAverageRTT(emails models.EmailRTTValues) int {

	totalRTT := 0

	for _, email := range emails {
		totalRTT += email.TransitTimeSeconds
	}

	return totalRTT / len(emails)
}
