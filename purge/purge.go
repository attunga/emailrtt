package purge

import (
	"emailrtt/common"
	"fmt"
	"github.com/luksen/maildir"
	"github.com/spf13/cobra"
	"io/ioutil"
	"net/mail"
	"os"
	"strconv"
	"strings"
	"time"
)

func Purge(cmd *cobra.Command, args []string) {

	// Initialise Configuration with Defaults
	config := common.Configuration{".", "development", "not set"}
	config = config.Load()

	fmt.Println("purge called")

	// Location of the Mail Dir of the user receiving emails
	var mailDir = maildir.Dir(config.MaildirFolder)

	// Get a listing of all of the emails in th e cur directory .. we don't want to touch the new files
	// as they have yet to be processed
	currentMessageKeys, err := mailDir.Keys()
	common.CheckErrorPanic("Error opening current message keys: %s \n", err)

	fmt.Println("Number of current messages:", len(currentMessageKeys))

	// If the number of emails found is less than 0 then bug out.
	if len(currentMessageKeys) < 1 {
		fmt.Println("No message found to process in the current directory")
		os.Exit(0)
	}

	// Process Removing Older Emails
	if cmd.Flag("remove").Value.String() == "true" {
		fmt.Println("Removing emails oder than", cmd.Flag("days").Value.String(), "days ...")

		deleteBeforeDays, err := strconv.Atoi(cmd.Flag("days").Value.String())
		common.CheckErrorPanic("Error trying to convert the days parameter: %s \n", err)

		var numberRemoved int = removeEmails(config, currentMessageKeys, mailDir, deleteBeforeDays)
		fmt.Println("\nEmails Deleted:", numberRemoved)
	}

	// Process Trimming Attachments
	if cmd.Flag("trim-attachments").Value.String() == "true" {
		fmt.Println("Trimming Attachments ...")
		var numberRemoved int = removeAttachments(config, currentMessageKeys, mailDir)
		fmt.Println("\nEmail Attachments Trimmed:", numberRemoved)
	}

	// function to remove old emails = should open the email - read the date and remove after a certain date.

	// This function may need optimisation to organise common operations into shared functions .....

}
func removeEmails(config common.Configuration, currentMessageKeys []string, mailDir maildir.Dir, deleteBeforeDays int) int {

	currentDate := time.Now()

	// Work out the date that we purge before
	purgeDate := currentDate.AddDate(0, 0, deleteBeforeDays*-1)
	purgeDate = purgeDate.Truncate(24 * time.Hour)

	fmt.Println("PurgeDate", purgeDate)

	numberProcessed := 0

	for _, messageKey := range currentMessageKeys {

		// We need the filename in case we are going to delete things
		filename, _ := mailDir.Filename(messageKey)

		// Get the header and then the header to extract the email received time
		emailHeader, _ := mailDir.Header(messageKey)
		receivedTime := getReceivedTime(emailHeader)

		if receivedTime.Before(purgeDate) {
			fmt.Println("Removing Email:", filename)
			err := os.Remove(filename)
			common.CheckErrorPanic("Error Removing File: %s \n", err)
			numberProcessed++
		}
	}

	return numberProcessed
}

// Remove the 50M attachments from the emails in the cur directory
func removeAttachments(config common.Configuration, currentMessageKeys []string, mailDir maildir.Dir) int {

	numberProcessed := 0

	for _, messageKey := range currentMessageKeys {

		filename, _ := mailDir.Filename(messageKey)

		//fmt.Println("Nessage Key:", messageKey)
		//fmt.Println("Filename:", filename)

		if removeAttachmentFromFile(filename) {
			numberProcessed++
		}

	}

	return numberProcessed
}

// Returns true if we stripped attachments .. otherwise false
func removeAttachmentFromFile(filename string) bool {

	// Open the  File into a series of bytes
	fileBytes, err := ioutil.ReadFile(filename) // just pass the file name
	common.CheckErrorPanic("Error opening file:%s \n", err)

	// Convert the by file bytes into a String
	emailText := string(fileBytes) // convert content to a 'string'

	// Get the file Size - the logic is that if the file is less then 5 meg in size them attachments have most likely
	// already been removed.
	fileSize := len(emailText)

	// Return false if the file is under 5 Megs,  most processed files are under 2 megs
	if fileSize < 5000 {
		return false
	}

	// Print Dots to give progress
	fmt.Print(".")

	// Attachments seem to have this line which we will remove
	attachmentLine := "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n"

	// Replace the Attachment line with a blank line
	newEmail := strings.Replace(emailText, attachmentLine, "", -1)

	//fmt.Println(newEmail) // print the content as a 'string'

	// Write the new file back again overwriting the original file
	newFile, err := os.Create(filename)
	common.CheckErrorPanic("Error creating file:%s \n", err)

	// Ensure that the file is closed even if we crash
	defer newFile.Close()

	// Write New Files
	_, err = newFile.WriteString(newEmail)
	common.CheckErrorPanic("Error writing data to file:%s \n", err)

	// Make sure that the file syncs to disk
	newFile.Sync()

	// if we get to here then we must have procesed a file so return true.
	return true
}

func getReceivedTime(receivedHeader mail.Header) time.Time {

	headerReceivedTimeDate := ""

	for key, value := range receivedHeader {
		//fmt.Println("Key", key, "Value", value)

		if key == "Received" {
			headerReceivedTimeDate = value[0]
		}
	}

	// If we have nothing for the headerReceivedTimeDate then we have a serious issue

	receivedHeadersSplit := strings.Split(headerReceivedTimeDate, ";")

	splitTime := strings.Trim(receivedHeadersSplit[1], " ")

	layout := "Mon, 02 Jan 2006 15:04:05 -0700 (MST)"
	t, err := time.Parse(layout, splitTime)
	if err != nil {
		fmt.Println(err)
	}

	return t
}
