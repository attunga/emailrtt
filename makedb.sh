#!/bin/bash

# Does Database Creation
/home/lindsay/go/bin/soda drop -a
/home/lindsay/go/bin/soda create -a


# Creates Code Files and Migrations
/home/lindsay/go/bin/soda generate model emailRTTValues \
					 messageKey:text \
                        		 emailFrom:text \
                        		 agency:text \
                        		 emailTo:text \
                        		 sentTimeDate:time \
	                		 receivedTimeDate:time \
	                		 receivedMonth:text \
	                		 receivedYear:int  \
	                		 transitTimeSeconds:int ;


# Creates the tables in the actual databases - development is default.

# Migrate Development
/home/lindsay/go/bin/soda migrate -e development up

# Migrate Production
/home/lindsay/go/bin/soda migrate -e production up









