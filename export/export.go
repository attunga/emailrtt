// Copyright © 2018 Lindsay Steele <lgsteele@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package export

import (
	"emailrtt/common"
	"emailrtt/models"
	"encoding/csv"
	"fmt"
	"github.com/gobuffalo/pop"
	"github.com/spf13/cobra"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

func ExportTo(cmd *cobra.Command, args []string) {

	// Should there be a parameters to pass for all? .. could be useful in spreadsheets

	// Check the variables passed for year and month is valid. .. we may convert years and months to valid numbers
	// Such as abreviations .. numbers .. full numbers etc etc

	// if parameters are passed for Month and Year then we try to parse then ...

	// If Parsing fails then we trying to get last months figures

	// if no Parameters passed then we try to get the figures for last month

	// Temp Variables
	month := cmd.Flag("month").Value.String()
	year := cmd.Flag("year").Value.String()

	//var month, year string = getPreviousMonthYear()

	//fmt.Println("Month:", month)
	//fmt.Println("Year:", year)

	// Get a struct object
	EmailsFound := models.EmailRTTValues{}

	// Update the Struct from the database ..
	// FIXME - DBstatus is most likely redundant and will never be called due to panic's exiting
	dbstatus := getEmailsFromDB(&EmailsFound, month, year)
	fmt.Println(dbstatus)

	// Check if there were actually any records returned .. if not then exit out gracefully
	//fmt.Println("Emails Found now outside:", len(EmailsFound))
	if len(EmailsFound) < 1 {
		fmt.Println("No Messages Found for Year and Month Given")
		os.Exit(0)
	}

	// Sort the emails Found by the Date Send Descending
	sort.SliceStable(EmailsFound, func(i, j int) bool { return EmailsFound[i].SentTimeDate.Before(EmailsFound[j].SentTimeDate) })

	switch strings.ToLower(cmd.Flag("type").Value.String()) {
	case "screen":
		exportToScreen(EmailsFound)
	case "csv":
		exportToCSV(EmailsFound)
	case "file":
		exportToFile(EmailsFound)
	case "html":
		exportToHTML(EmailsFound)
	case "email":
		//exportToEmail()
	default:
		fmt.Printf("Unknown type given:", cmd.Flag("type").Value.String())
	}

	// From here we should get the parameters to work out the search type we want .. month and  year basically.

	// We we then pass the parameters off to a DB Object which does the search and returns the struct of records

	// We then use a Case Statement to parse off the selected export type that can be changed in the future

}
func getPreviousMonthYear() (string, string) {

	currentTime := time.Now()

	currentYear := strconv.Itoa(currentTime.Year())
	previousMonth := (currentTime.Month() - 1).String()

	return previousMonth, currentYear
}

func exportToCSV(EmailsFound models.EmailRTTValues) {
	fmt.Println("Export to CSV Called")

	filename := "rtt.csv"

	file, err := os.Create(filename)
	common.CheckErrorPanic("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	var csvline = []string{"Agency", "Transit Time", "Date/Time Sent", "Date/Time Received", "Email From", "Email To"}
	err = writer.Write(csvline)
	common.CheckErrorPanic("Cannot write to file", err)

	utclocation, _ := time.LoadLocation("Australia/Sydney")

	for _, email := range EmailsFound {

		csvline = []string{email.Agency,
			strconv.Itoa(email.TransitTimeSeconds),
			email.SentTimeDate.In(utclocation).String(),
			email.ReceivedTimeDate.In(utclocation).String(),
			email.EmailFrom,
			email.EmailTo}
		err := writer.Write(csvline)
		common.CheckErrorPanic("Cannot write to file", err)
	}

}

func exportToFile(EmailsFound models.EmailRTTValues) {
	fmt.Println("Export to File Called")

}

func exportToHTML(EmailsFound models.EmailRTTValues) {
	fmt.Println("Export to HTML Called")

}

func exportToScreen(EmailsFound models.EmailRTTValues) {

	utclocation, _ := time.LoadLocation("Australia/Sydney")

	for _, emailRTT := range EmailsFound {

		// Just a debug
		//fmt.Println(utclocation)
		fmt.Print("Message Key:", emailRTT.MessageKey)
		fmt.Print("Agency:", emailRTT.Agency)
		fmt.Print("Sent Time:", emailRTT.SentTimeDate.In(utclocation))
		fmt.Print("ReceivedTime:", emailRTT.ReceivedTimeDate.In(utclocation))
		fmt.Println("Transit Time Seconds", emailRTT.TransitTimeSeconds)
	}

}
func getEmailsFromDB(EmailsFound *models.EmailRTTValues, month string, year string) error {

	// Initialise Configuration with Defaults
	config := common.Configuration{".", "development", "not set"}
	config = config.Load() // Hopefully this loads the config

	// Do a database connection.  Database Type refers to Production, Test or Development
	db, err := pop.Connect(config.DatabaseType)
	common.CheckErrorPanic("Fatal error cannot connect to database: %s \n", err)

	err = db.Open()
	common.CheckErrorPanic("Fatal error cannot open database: %s \n", err)
	defer db.Close()

	// Do a query on the database for the Month and year of the RTT Records that we want
	err = db.Where("received_month = ?", month).Where(" received_year = ?", year).All(EmailsFound)
	common.CheckErrorPanic("Fatal error cannot do Query: %s \n", err)

	fmt.Println("Emails Found now inside:", len(*EmailsFound))

	err = db.Close()
	common.CheckErrorPanic("Fatal error cannot close database: %s \n", err)

	return nil

}
