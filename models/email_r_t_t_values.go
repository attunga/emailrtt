package models

import (
	"encoding/json"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"time"
)

type EmailRTTValue struct {
	ID                 uuid.UUID `json:"id" db:"id"`
	CreatedAt          time.Time `json:"created_at" db:"created_at"`
	UpdatedAt          time.Time `json:"updated_at" db:"updated_at"`
	MessageKey         string    `json:"message_key" db:"message_key"`
	EmailFrom          string    `json:"email_from" db:"email_from"`
	Agency             string    `json:"agency" db:"agency"`
	EmailTo            string    `json:"email_to" db:"email_to"`
	SentTimeDate       time.Time `json:"sent_time_date" db:"sent_time_date"`
	ReceivedTimeDate   time.Time `json:"received_time_date" db:"received_time_date"`
	ReceivedMonth      string    `json:"received_month" db:"received_month"`
	ReceivedYear       int       `json:"received_year" db:"received_year"`
	TransitTimeSeconds int       `json:"transit_time_seconds" db:"transit_time_seconds"`
}

// String is not required by pop and may be deleted
func (e EmailRTTValue) String() string {
	je, _ := json.Marshal(e)
	return string(je)
}

// EmailRTTValues is not required by pop and may be deleted
type EmailRTTValues []EmailRTTValue

// String is not required by pop and may be deleted
func (e EmailRTTValues) String() string {
	je, _ := json.Marshal(e)
	return string(je)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (e *EmailRTTValue) Validate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.Validate(
		&validators.StringIsPresent{Field: e.MessageKey, Name: "MessageKey"},
		&validators.StringIsPresent{Field: e.EmailFrom, Name: "EmailFrom"},
		&validators.StringIsPresent{Field: e.Agency, Name: "Agency"},
		&validators.StringIsPresent{Field: e.EmailTo, Name: "EmailTo"},
		&validators.TimeIsPresent{Field: e.SentTimeDate, Name: "SentTimeDate"},
		&validators.TimeIsPresent{Field: e.ReceivedTimeDate, Name: "ReceivedTimeDate"},
		&validators.StringIsPresent{Field: e.ReceivedMonth, Name: "ReceivedMonth"},
		&validators.IntIsPresent{Field: e.ReceivedYear, Name: "ReceivedYear"},
		&validators.IntIsPresent{Field: e.TransitTimeSeconds, Name: "TransitTimeSeconds"},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (e *EmailRTTValue) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (e *EmailRTTValue) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
